const HOOK_SETTINGS = { order: 999999, filter: { fake: null, incoming: null, silenced: null, modified: null } }
const fs = require('fs')
const path = require("path")
const hexy = require('hexy')
const UI = require('tera-mod-ui').Settings

if (!fs.existsSync(path.join(__dirname, 'log'))) {
	fs.mkdirSync(path.join(__dirname, 'log'));
}
function filename() {
	var time = new Date();
	return `${time.getFullYear().toString().padStart(4, '0')
	}-${time.getMonth().toString().padStart(2, '0')
	}-${time.getDate().toString().padStart(2, '0')
	}_${time.getHours().toString().padStart(2, '0')
	}-${time.getMinutes().toString().padStart(2, '0')
	}-${time.getSeconds().toString().padStart(2, '0')}.log`
}
function timestamp() {
	var time = new Date()
	return `[${time.getHours().toString().padStart(2, '0')
	}:${time.getMinutes().toString().padStart(2, '0')
	}:${time.getSeconds().toString().padStart(2, '0')
	}.${time.getMilliseconds().toString().padStart(3, '0')}] `
}

exports.NetworkMod = function(mod) {
	let custom = require('./set_custom.js'), hook = null, ws = null
	if (!global.TeraProxy.GUIMode) return
	let ui = new UI(mod, require('./set_structure'), mod.settings)
	ui.on('update', settings => {
		if ( mod.settings.logC ||  mod.settings.logS) enable()
		if (!mod.settings.logC && !mod.settings.logS) disable()
		
		delete require.cache[require.resolve('./set_custom.js')]
		custom = require('./set_custom.js')
	})
	this.destructor = () => {
		if (ui) ui.close()
		ui = null
		if (ws) ws.end()
		ws = null
	}
	mod.command.add("log", () => ui.show())
	
	if (mod.settings.logC || mod.settings.logS) enable()
	function enable() {
		if (hook) return
		ws = fs.createWriteStream(path.join(__dirname, 'log', filename()), {flags:"a"})
		log('<---- Hook ENABLED ---->')
		hook = mod.hook('*', 'raw', HOOK_SETTINGS, (code, data, incoming, fake) => {
			if (incoming==mod.settings.logC && incoming!=mod.settings.logS) return
			
			if (custom.opcodes.includes(code)) return
			
			var name, version, packet, packaged
			name = mod.dispatch.protocolMap.code.get(code)
			switch (mod.settings.logF) {
				case "Only":
					if (!custom.protocol_only.includes(name)) return
					break
				case "Pass":
					if ( custom.protocol_pass.includes(name)) return
					break
			}
			
			if (name==undefined) {
log(`OpCode:${code}|${arrow(incoming)}|${type(data)} length:4+${data.length-4}`)
			} else {
				version = mod.dispatch.latestDefVersion.get(name)
				if (version==undefined) {
log(`OpCode:${code}|${arrow(incoming)}|${type(data)} ${name} length:4+${data.length-4}`)
				} else {
					packet = mod.dispatch.fromRaw(code, '*', data)
					packaged = mod.dispatch.toRaw(name, '*', packet)
					if (!packet || (data.length != packaged.length) || !data.equals(packaged)) {
log(`OpCode:${code}|${arrow(incoming)}|${type(data)} ${name} v${version} length:4+${data.length-4} [!!! def_x !!!]`)
					} else {
						// 这是一条拥有 映射码-协议名称和版本-且数据包长度检测合格的 TERA数据包
						if (mod.settings.logF=="Error") return
log(`OpCode:${code}|${arrow(incoming)}|${type(data)} ${name} v${version} length:4+${data.length-4} [def_√]`)
					}
				}
			}
			
			if (mod.settings.logJ && packet) {
				loopBigIntToString(packet)
				mod.log(`JSON: ${JSON.stringify(packet, null, 4)}`)
			}
			
			if (mod.settings.logR) {
log(`Raw: ${data.toString('hex').slice(0, 4)} ${data.toString('hex').slice(4, 8)} ${hexDump(data).slice(8)}`)
				if (packaged && ((data.length != packaged.length) || !data.equals(packaged))) {
log(`Pac: ${packaged.toString('hex').slice(0, 4)} ${packaged.toString('hex').slice(4, 8)} ${hexDump(packaged).slice(8)}`)
				}
			}
		})
	}
	function log(msg) {
		mod.log(msg)
		ws.write(timestamp()+msg+`\n`)
	}
	function disable() {
		if (!hook) return
		mod.unhook(hook)
		hook = null
		log('<---- Hook DISABLED ---->')
		ws.end()
		ws = null
	}
	function loopBigIntToString(obj) {
		Object.keys(obj).forEach(key => {
			if (obj[key] && typeof obj[key] === 'object') {
				loopBigIntToString(obj[key])
			} else if (typeof obj[key] === "bigint") {
				obj[key] = obj[key].toString()
			}
		})
	}
	function arrow(incoming) {
		return incoming ? 'Server' : 'Client'
	}
	function type(data) {
		return data.$fake?`伪造`: data.$silenced?`阻断` : data.$modified?`修改` : `----`
	}
	function hexDump(data) {
		if (mod.settings.logP === "String") {
			return data.toString('hex')
		} else {
			return hexy.hexy(data, {format: mod.settings.logP, offset: 4, caps: "upper", width: 32})
		}
	}
}
