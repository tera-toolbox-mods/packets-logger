module.exports = [
	{key: "logC", name: `Client Packet`, type: "bool"},
	{key: "logS", name: `Server Packet`, type: "bool"},
	{key: "logF", name: `Filter Packet`, type: "select",
		options: [
			{name: "protocol_only", key: "Only"},
			{name: "protocol_pass", key: "Pass"},
			{name: "Wrong | Unknown", key: "Error"},
			{name: "Filter None",   key: "None"},
		]
	},
	{key: "logJ", name: `Packet => JSON`, type: "bool"},
	{key: "logR", name: `Packet => Raw`, type: "bool"},
	{key: "logP", name: `Raw-Style`, type: "select",
		options: [
			{name: "String", key: "String"},
			{name: "Hexy 2", key: "twos"},
			{name: "Hexy 4", key: "fours"},
			{name: "Hexy 8", key: "eights"},
			{name: "Hexy 16", key: "sixteens"},
			{name: "Hexy none", key: "none"}
		]
	}
]
